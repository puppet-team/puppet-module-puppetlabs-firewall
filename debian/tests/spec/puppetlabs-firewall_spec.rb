require 'spec_helper'

describe "iptables is not installed" do
  describe package('iptables') do
    it { should_not be_installed }
  end
end

describe "test classes apply successfully" do
  describe command("puppet apply --logdest /var/log/puppet-apply.log --test spec/fixtures/test.pp") do
    its(:exit_status) { should eq 2 }
  end
end

describe "iptables is installed" do
  describe package('iptables') do
    it { should be_installed }
  end
end

describe "netfilter-persistent is enabled and running" do
  describe service('netfilter-persistent') do
    it { should be_enabled }
    it { should be_running }
  end
end

describe "expected ipv4 rules are loaded" do
  describe command('iptables-save') do
    its(:stdout) { should include "-A INPUT -p icmp -m comment --comment \"000 fwrules::ipv4 accept all icmp\" -j ACCEPT" }
    its(:stdout) { should include "-A INPUT -i lo -m comment --comment \"001 fwrules::ipv4 accept all to lo interface\" -j ACCEPT" }
    its(:stdout) { should include "-A INPUT -m state --state RELATED,ESTABLISHED -m comment --comment \"003 fwrules::ipv4 accept related established rules\" -j ACCEPT" }
    its(:stdout) { should include "-A INPUT -s 192.0.2.0/24 -p tcp -m multiport --dports 80,443 -m comment --comment \"0101 profile::appserver legacy admin access\" -j ACCEPT" }
  end
end

describe "expected ipv6 rules are loaded" do
  describe command('ip6tables-save') do
    its(:stdout) { should include "-A INPUT -p ipv6-icmp -m comment --comment \"000 fwrules::ipv6 accept all icmp\" -j ACCEPT" }
    its(:stdout) { should include "-A INPUT -i lo -m comment --comment \"001 fwrules::ipv6 accept all to lo interface\" -j ACCEPT" }
    its(:stdout) { should include "-A INPUT -m state --state RELATED,ESTABLISHED -m comment --comment \"003 fwrules::ipv6 accept related established rules\" -j ACCEPT" }
    its(:stdout) { should include "-A INPUT -s 2001:db8:de:caf::/64 -p tcp -m multiport --dports 540 -m comment --comment \"0100 profile::appserver Future-proof XMLRPC over UUCP over IPv6 appserver traffic\" -j ACCEPT" }
    its(:stdout) { should include "-A INPUT -s 2001:db8:c0f:fee::/64 -p tcp -m multiport --dports 80,443 -m comment --comment \"0101 profile::appserver admin access\" -j ACCEPT" }
  end
end
