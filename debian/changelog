puppet-module-puppetlabs-firewall (8.0.0-3) unstable; urgency=medium

  * Team upload.
  * d/control: fix missing dep on ruby-puppet-resource-api

 -- Jérôme Charaoui <jerome@riseup.net>  Sat, 24 Feb 2024 23:15:02 -0500

puppet-module-puppetlabs-firewall (8.0.0-2) unstable; urgency=medium

  * Team upload.
  * d/control: require more recent stdlib module
  * d/tests: use rspec documentation format

 -- Jérôme Charaoui <jerome@riseup.net>  Sat, 24 Feb 2024 15:17:29 -0500

puppet-module-puppetlabs-firewall (8.0.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 8.0.0
  * d/patches:
    - cleanup empty folder
    + puppet is now puppet-agent
    + bump dh-compat to 13
    + bump Standards-Version, no changes needed
  * d/patches: fix ip6tables protocol in firewallchain
  * d/tests: ignore documentation lint errors
  * d/tests: update test manifest for new module version
  * d/tests: convert to serverspec unit tests
  * d/upstream: add Bug-* and Changelog metadata
  * run wrap-and-sort -bastk
  * ship REFERENCE.md with docs

 -- Jérôme Charaoui <jerome@riseup.net>  Fri, 23 Feb 2024 22:02:21 -0500

puppet-module-puppetlabs-firewall (3.4.0-1) unstable; urgency=medium

  * New upstream release.
  * Fix README.markdown -> README.md in
    d/puppet-module-puppetlabs-firewall.docs.

 -- Thomas Goirand <zigo@debian.org>  Tue, 24 Jan 2023 12:35:50 +0100

puppet-module-puppetlabs-firewall (1.12.0-1) unstable; urgency=medium

  [ Thomas Bechtold ]
  * Remove myself from Uploaders (Closes: #892667)

  [ Sebastien Badia ]
  * New upstream version 1.12.0
  * d/control:
    + Bump to Standards-Version 4.1.3 (no changes needed)
    + Use salsa.debian.org in Vcs-* fields
    + Added myself as Uploader
    + Add missing dependency on stdlib for pick function (Closes: #893631)
  * d/upstream: Added Upstream metadata
  * d/watch: Bump to version 4 and use HTTPS for URI
  * d/copyright: Update copyright years and use HTTPS for URI
  * d/docs: Add missing NOTICE file (for Apache2 license)

 -- Sebastien Badia <sbadia@debian.org>  Wed, 21 Mar 2018 22:05:21 +0100

puppet-module-puppetlabs-firewall (1.11.0-1) unstable; urgency=medium

  * Team upload.
  * Imported upstream release 1.11.0.
  * Depend on puppet instead of puppet-common.
  * Add Rules-Requires-Root: no.
  * Run wrap-and-sort -ast.
  * Remove myself from Uploaders.
  * Fix ordering of stanzas in debian/copyright.
  * Update to debhelper compatibility level V11.
  * Update standards version to 4.1.2 (no changes required).

 -- Russ Allbery <rra@debian.org>  Wed, 20 Dec 2017 20:17:19 -0800

puppet-module-puppetlabs-firewall (1.8.1-1) unstable; urgency=medium

  * Imported upstream relase 1.8.1

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Tue, 24 May 2016 23:05:45 +0200

puppet-module-puppetlabs-firewall (1.8.0-1) unstable; urgency=medium

  * Imported upstream relase 1.8.0
  * Declare compliance with Debian policy 3.9.8
  * Update VCS-* URLs
  * Add myself to Uploaders list
  * Update source homepage
  * Run autopkgtests in isolation container

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Tue, 17 May 2016 13:51:36 +0200

puppet-module-puppetlabs-firewall (1.7.1-1) unstable; urgency=medium

  * Team upload.
  * Imported upstream version 1.7.1
  * [ea70c2d] Add DEP-8 tests
  * [1f6780a] Declare compliance with standards version 3.9.6
  * [89a285d] Update VCS URIs

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Sat, 28 Nov 2015 21:36:31 +0100

puppet-module-puppetlabs-firewall (1.1.3-1) unstable; urgency=medium

  * New upstream release.
    - Add support for connlimit and connmark.
    - Add mask as a parameter.
    - Add systemd support for RHEL7.
  * Add a Debian patch to support the change of init script name to
    netfilter-persistent introduced in iptables-persistent 1.0.
    (Closes: #748425)
  * No longer install Modulefile, since this has been dropped upstream.
  * Canonicalize the Vcs-Git URL.
  * Add myself to Uploaders.

 -- Russ Allbery <rra@debian.org>  Sat, 23 Aug 2014 23:36:53 -0700

puppet-module-puppetlabs-firewall (1.0.2-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Bechtold <toabctl@debian.org>  Fri, 07 Mar 2014 21:24:14 +0100

puppet-module-puppetlabs-firewall (0.4.2-2) unstable; urgency=medium

  * debian/watch: use redirector from qa.debian.org.
  * debian/control:
    - use Debian address in Uploaders field.
    - Bump Standards-Version to 3.9.5. No further changes.

 -- Thomas Bechtold <toabctl@debian.org>  Wed, 22 Jan 2014 07:51:42 +0100

puppet-module-puppetlabs-firewall (0.4.2-1) unstable; urgency=low

  [ Thomas Bechtold ]
  * Initial release (Closes: #724012)

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Mon, 14 Oct 2013 10:53:12 +0200
